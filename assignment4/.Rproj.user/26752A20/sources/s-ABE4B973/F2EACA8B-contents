# Mit source() wird das Package "bioclite" von der folgenden URL importiert:
# source("https://bioconductor.org/biocLite.R")
# sbiocLite("rGADEM")

# Load package: die Libary "rGADEM" wird aus dem Package "bioclite" geladen
library("rGADEM")

# Die Variable "newsequences" wird mit dem Ergebnis der Funktion readDNAStringSet() initialisiert. Diese Funktion liest eine Datei im FASTA-Format ein und liefert die 
# darin enthaltenen DNA-Sequenzen in einem DNAStringSet-Objekt zurück. In der Datei "Test_ScriptData.fasta" sind die Beispielsequenzen aus Folie 9 der 6. Vorlesung
# hinterlegt.
# Referenz für readDNAStringSet(): https://www.rdocumentation.org/packages/Biostrings/versions/2.40.2/topics/XStringSet-io
newsequences <- readDNAStringSet("Test_ScriptData.fasta", "fasta")

# Die Funktion GADEM ist Teil des "rGADEM"-Packages und dient zum Finden häufig vorkommender Motive. Die Variable "gadem" wird mit dem Ergebnis der GADEM-Funktion 
# initialisiert. Mit "verbose=1" wird veranlasst, dass die Zwischenergebnisse des Funktionsaufrufes in der Konsole ausgegeben werden.
# Referenz für GADEM(): https://www.rdocumentation.org/packages/rGADEM/versions/2.20.0/topics/GADEM
gadem <- GADEM(newsequences, verbose=1)

# Im Objekt "gadem" sind nun die am häufigsten vorkommenden Motive für die einzelnen Sequenzen aus der Datei "Test_ScriptData.fasta" hibterlegt. Um aus diesen einzelnen 
# Motiven ein Konsensusmotiv zu erhalten, wird die Funktion consensus() aud das Objekt "gadem" angewandt. Neben der reinen Sequenzinformation für das Konsensusmotiv
# enthält die Rückgabe zusätzlich Informationen über die Wahrscheinlichkeit alternativer Basen an den jeweiligen Stellen des Motivs.
# Referenz für conesensus(): https://www.rdocumentation.org/packages/rGADEM/versions/2.20.0/topics/gadem-class
consensus(gadem)
pwm <- gadem@motifList[[1]]@pwm

# Abschließend wird das ermittelte Konsensusmotiv graphisch in Form eines sogenannten "Motif Logo" ausgegeben, bei dem die Größe der Basensymbole dem Informations-
# gehalt (der mit der Wahrscheinlichkeit des Auftretens einer bestimmten Base an der Stelle korrespondiert) entspricht.
# Referenz für setLogo(): https://www.rdocumentation.org/packages/seqLogo/versions/1.38.0/topics/seqLogo
seqLogo(pwm)