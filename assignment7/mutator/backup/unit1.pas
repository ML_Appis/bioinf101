unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ExtCtrls;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    GB_Type: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    M_Input: TMemo;
    M_Output: TMemo;
    Panel1: TPanel;
    RB_Protein: TRadioButton;
    RB_DNA: TRadioButton;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);

  private
    procedure MutateDNA();
    procedure MutateProtein();

  public

  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.Button1Click(Sender: TObject);
begin
  IF RB_DNA.Checked THEN MutateDNA() ELSE MutateProtein();
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  Randomize;
end;

procedure TForm1.MutateDNA();
var input_seq, output_seq: STRING;
    i, p, c: Integer;
begin
  input_seq := M_Input.Lines.Text;
  output_seq := input_seq;
  c := 0;
  FOR i := 1 TO Length(input_seq) DO
  begin
    p := Random(99)+1;
    CASE p OF
      14..15: begin
      Delete(output_seq, i, 1);
      output_seq:= output_seq + ' ';
      Inc(c);
      end;
      2..4: output_seq[i] := 'A';
      5..7: output_seq[i] := 'T';
      8..10: output_seq[i] := 'C';
      11..13: output_seq[i] := 'G';
    end;
  end;
  M_Output.Text := output_seq;
end;

procedure TForm1.MutateProtein();
VAR seq: STRING;
    i, c: Integer;
begin
  seq := M_Input.Text;
  i := 0;
  WHILE (i < Length(seq)) DO
  begin
    c := Random(999)+1;
    M_Output.Lines.Add(IntToStr(c));
    CASE c OF
      1..20: Delete(seq, i, 1);
      21..25: seq[i] := 'A';  // 1 Alanin
      26..30: seq[i] := 'G';  // 2 Glycin
      31..35: seq[i] := 'L';  // 3 Leucin
      36..40: seq[i] := 'I';  // 4 Isoleucin
      41..45: seq[i] := 'Y';  // 5 Tyrosin
      46..50: seq[i] := 'W';  // 6 Tryptophan
      51..55: seq[i] := 'F';  // 7 Phenylalanin
      56..60: seq[i] := 'P';  // 8 Prolin
      61..65: seq[i] := 'S';  // 9 Serin
      66..70: seq[i] := 'K';  // 10 Lysin
      71..75: seq[i] := 'M';  // 11 Methionin
      76..80: seq[i] := 'N';  // 12 Asparagin
      81..85: seq[i] := 'D';  // 13 Asparaginsäure
      86..90: seq[i] := 'Q';  // 14 Glutamin
      91..95: seq[i] := 'E';  // 15 Glutaminsäure
      96..100: seq[i] := 'R';  // 16 Arginin
      101..105: seq[i] := 'H';  // 17 Histidin
      106..110: seq[i] := 'V';  // 18 Valin
      111..115: seq[i] := 'C';  // 19 Cystein
      116..120: seq[i] := 'T';  // 20 Threonin
      // 121..124: Insert('A', seq, i);
    end;
    Inc(i);
  end;
  M_Output.Text := seq;
end;

end.

